// // // CHANGE THIS // // //
const themeName = 'replace-me';

// REQUIRE PLUGINS
const gulp = require('gulp');
const sass = require('gulp-sass');
const watch = require('gulp-watch');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync');
const eslint = require('gulp-eslint');


//LOCAL DEVELOPMENT
gulp.task('theme-sass', function (done) {
  gulp.src('wp-content/themes/' + themeName + '/css/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('wp-content/themes/' + themeName));
  done();
});

gulp.task('theme-sass-login', function (done) {
  gulp.src('wp-content/themes/' + themeName + '/admin/mxb-login-style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('wp-content/themes/' + themeName + '/admin/'));
  done();
});

gulp.task('lint-js', function (done) {
  return gulp.src(['wp-content/themes/' + themeName + '/js/main.js'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
  done();
});

gulp.task('watch', function (done) {
  browserSync.init({
    proxy: "http://localhost/projects/" + themeName
  });
  gulp.watch('wp-content/themes/' + themeName + '/css/**/*.scss', gulp.series('theme-sass'));
  gulp.watch('wp-content/themes/' + themeName + '/admin/**/*.scss', gulp.series('theme-sass-login'));
  gulp.watch('wp-content/themes/' + themeName + '/js/main.js', gulp.series('lint-js'));
  gulp.watch(['wp-content/themes/' + themeName + '/style.css', 'wp-content/themes/' + themeName + '/**/*.php', 'wp-content/themes/' + themeName + '/js/main.js']).on('change', browserSync.reload);
  done();
});